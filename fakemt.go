package main

import (
	"flag"
	"fmt"
	"os"
	"time"
)

func version() {
	os.Stderr.WriteString(`fakemt: v0.1.0
`)
}

func usage() {
	os.Stderr.WriteString(`
Usage: fakemt [OPTION] FILE...
Change timestamp of FILE(s)

Options:
	--year   -Y  new year
	--month  -M  new month
	--day    -D  new day
	--hour   -h  new hour
	--month  -m  new month
	--second -s  new second
	--nano   -n  new nanosecound
	--help       show this help message
	--version    print the version

Report bugs to <kusabashira227@gmail.com>
`[1:])
}

type Time struct {
	Year        int
	Month       int
	Day         int
	Hour        int
	Mintue      int
	Second      int
	Nanosecound int
}

func getNewTime(from time.Time, chTime Time) time.Time {
	if chTime.Year == -1 {
		chTime.Year = from.Year()
	}
	if chTime.Month == -1 {
		chTime.Month = int(from.Month())
	}
	if chTime.Day == -1 {
		chTime.Day = from.Day()
	}
	if chTime.Hour == -1 {
		chTime.Hour = from.Hour()
	}
	if chTime.Mintue == -1 {
		chTime.Mintue = from.Minute()
	}
	if chTime.Second == -1 {
		chTime.Second = from.Second()
	}
	if chTime.Nanosecound == -1 {
		chTime.Nanosecound = from.Nanosecond()
	}
	return time.Date(
		chTime.Year,
		time.Month(chTime.Month),
		chTime.Day,
		chTime.Hour,
		chTime.Mintue,
		chTime.Second,
		chTime.Nanosecound,
		from.Location(),
	)
}

func overWriteTimeStamp(target string, chTime Time) error {
	finfo, err := os.Stat(target)
	if err != nil {
		return err
	}

	oriTime := finfo.ModTime()
	writeTime := getNewTime(oriTime, chTime)
	err = os.Chtimes(target, writeTime, writeTime)
	if err != nil {
		return err
	}
	return nil
}

func _main() (exitCode int, err error) {
	var newTime Time
	flag.IntVar(&newTime.Year, "Y", -1, "Year")
	flag.IntVar(&newTime.Year, "year", -1, "Year")
	flag.IntVar(&newTime.Month, "M", -1, "Month")
	flag.IntVar(&newTime.Month, "month", -1, "Month")
	flag.IntVar(&newTime.Day, "D", -1, "Day")
	flag.IntVar(&newTime.Day, "day", -1, "Day")
	flag.IntVar(&newTime.Hour, "h", -1, "Hour")
	flag.IntVar(&newTime.Hour, "hour", -1, "Hour")
	flag.IntVar(&newTime.Mintue, "m", -1, "Mintue")
	flag.IntVar(&newTime.Mintue, "minute", -1, "Mintue")
	flag.IntVar(&newTime.Second, "s", -1, "Second")
	flag.IntVar(&newTime.Second, "second", -1, "Second")
	flag.IntVar(&newTime.Nanosecound, "n", -1, "Nanosecound")
	flag.IntVar(&newTime.Nanosecound, "nano", -1, "Nanosecound")

	var isHelp, isVersion bool
	flag.BoolVar(&isHelp, "help", false, "show this help message")
	flag.BoolVar(&isVersion, "version", false, "print the version")

	flag.Usage = usage
	flag.Parse()

	if isHelp {
		usage()
		return 0, nil
	}

	if isVersion {
		version()
		return 0, nil
	}

	if flag.NArg() < 1 {
		return 1, fmt.Errorf("no target file")
	}
	for i := 0; i < flag.NArg(); i++ {
		err = overWriteTimeStamp(flag.Arg(i), newTime)
		if err != nil {
			return 1, err
		}
	}

	return 0, nil
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "fakemt:", err)
	}
	os.Exit(exitCode)
}
